import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseClient {
  static Future<Database>? _db;

  static Future<Database> get db async {
    if(_db == null) {
      await create();
    }
    return _db!;
  }

  static Future<void> create() async {
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path + 'weather_nals.db';
    _db ??= openDatabase(
      path,
      version: 1,
      onCreate: _onDatabaseCreate,
    );
  }

  static FutureOr<void> _onDatabaseCreate(Database db, int version) async {
    await db.execute("""
      CREATE TABLE weathers (
        id INTEGER PRIMARY KEY, 
        weather_state_name TEXT, 
        weather_state_abbr TEXT,
        applicable_date TEXT,
        the_temp DOUBLE,
        humidity INTEGER,
        predictability INTEGER
      );
      """);
  }
}
