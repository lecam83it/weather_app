import 'dart:io';

import 'package:dio/dio.dart';
import 'package:interview_nals/core/error/api_exception.dart';

class Client {
  final Dio dio;

  Client({
    required this.dio,
  });

  void onError(DioError err) {
    switch (err.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw DeadlineExceededException();
      case DioErrorType.response:
        switch (err.response?.statusCode) {
          case HttpStatus.badRequest:
            throw BadRequestException(message: err.response!.data["message"]);
          case HttpStatus.unauthorized:
            throw UnauthorizedException();
          case HttpStatus.notFound:
            throw NotFoundException();
          case HttpStatus.conflict:
            throw ConflictException();
          case HttpStatus.internalServerError:
            throw InternalServerErrorException();
        }
        break;
      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        throw NoInternetConnectionException();
    }
  }

  Future<Response<T>> get<T>(
    String path, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
  }) {
    return dio.get(
      path,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onReceiveProgress: onReceiveProgress,
    );
  }
}
