import 'package:interview_nals/core/error/api_exception.dart';
import 'package:interview_nals/core/platform/network_info.dart';
import 'package:interview_nals/features/weather/data/datasources/weather_local_data_source.dart';
import 'package:interview_nals/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:interview_nals/features/weather/data/models/weather_model.dart';
import 'package:interview_nals/features/weather/domain/entities/weather.dart';
import 'package:interview_nals/features/weather/domain/repositories/weather_repository.dart';

class WeatherRepositoryImpl extends WeatherRepository {
  final NetworkInfo networkInfo;
  final WeatherRemoteDataSource remoteDataSource;
  final WeatherLocalDataSource localDataSource;

  WeatherRepositoryImpl({
    required this.networkInfo,
    required this.remoteDataSource,
    required this.localDataSource,
  });

  @override
  Future<Weather?> getWeather(DateTime date) async {
    if(await networkInfo.isConnected) {
      final WeatherModel? weather = await remoteDataSource.getWeather(date);

      if(weather != null) {
        await localDataSource.cacheWeather(weather);
      }

      return weather;
    } else {
      final WeatherModel? weather = await localDataSource.getWeather(date);
      if(weather == null) {
        throw NoInternetConnectionException();
      }
      return weather;
    }
  }
}
