import 'package:interview_nals/features/utils/date_time_utils.dart';
import 'package:interview_nals/features/weather/domain/entities/weather.dart';

class WeatherModel extends Weather {
  WeatherModel({
    required int id,
    required String name,
    required String abbr,
    required DateTime applicableDate,
    required double temp,
    required int humidity,
    required int predictability,
  }) : super(
          id: id,
          name: name,
          abbr: abbr,
          applicableDate: applicableDate,
          humidity: humidity,
          predictability: predictability,
          temp: temp,
        );


  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
      id: json["id"],
      name: json["weather_state_name"],
      abbr: json["weather_state_abbr"],
      applicableDate: DateTimeExtension.parseToDate(json["applicable_date"]),
      temp: json["the_temp"],
      humidity: json["humidity"],
      predictability: json["predictability"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id" : this.id,
      "weather_state_name" : this.name,
      "weather_state_abbr" : this.abbr,
      "applicable_date" : this.applicableDate.toString(),
      "the_temp" : this.temp,
      "humidity" : this.humidity,
      "predictability" : this.predictability,
    };
  }
}
