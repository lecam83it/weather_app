import 'package:interview_nals/features/utils/date_time_utils.dart';
import 'package:interview_nals/features/weather/data/models/weather_model.dart';
import 'package:sqflite/sqflite.dart';

abstract class WeatherLocalDataSource {
  // get cached weather by date
  Future<WeatherModel?> getWeather(DateTime date);

  // cache remote weather model to persist
  Future<void> cacheWeather(WeatherModel model);
}

class WeatherLocalDataSourceImpl extends WeatherLocalDataSource {
  static const kWeatherTable = "weathers";
  final Database database;

  WeatherLocalDataSourceImpl({required this.database});

  @override
  Future<void> cacheWeather(WeatherModel model) async {
    final response = await database.query(
      kWeatherTable,
      where: 'applicable_date = ?',
      whereArgs: [model.applicableDate.toString()],
    );
    if (response.isEmpty) {
      await database.insert(kWeatherTable, model.toJson());
    } else {
      await database.update(
        kWeatherTable,
        model.toJson(),
        where: 'applicable_date = ?',
        whereArgs: [model.applicableDate.toString()],
      );
    }
  }

  @override
  Future<WeatherModel?> getWeather(DateTime date) async {
    final response = await database.query(
      kWeatherTable,
      where: 'applicable_date = ?',
      whereArgs: [date.toDate().toString()],
    );
    final List<WeatherModel> weathers =
        List.from(response).map((e) => WeatherModel.fromJson(e)).toList();

    if (weathers.isNotEmpty) {
      return weathers.first;
    }

    return null;
  }
}
