import 'package:dio/dio.dart';
import 'package:interview_nals/core/platform/client.dart';
import 'package:interview_nals/features/utils/date_time_utils.dart';
import 'package:interview_nals/features/weather/data/models/weather_model.dart';

abstract class WeatherRemoteDataSource {
  Future<WeatherModel?> getWeather(DateTime date);
}

class WeatherRemoteDataSourceImpl extends WeatherRemoteDataSource {
  static const kWeatherPath = "https://www.metaweather.com/api/location/1252431";

  final Client client;

  WeatherRemoteDataSourceImpl({
    required this.client,
  });

  @override
  Future<WeatherModel?> getWeather(DateTime date) async {
    try {
      var url = "$kWeatherPath/${date.toSlashDate()}";
      final response = await client.get(url);

      final List<WeatherModel> weathers = List.from(response.data)
          .map((e) => WeatherModel.fromJson(e))
          .toList();

      if (weathers.isNotEmpty) {
        return weathers.first;
      }

      return null;
    } on DioError catch (dioError) {
      client.onError(dioError);
    }
  }
}
