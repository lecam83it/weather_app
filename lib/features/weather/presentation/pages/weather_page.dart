import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_dimen_theme.dart';
import 'package:interview_nals/features/components/ui_home_indicator.dart';
import 'package:interview_nals/features/components/ui_snackbar.dart';
import 'package:interview_nals/features/components/ui_status_bar.dart';
import 'package:interview_nals/features/shared/ui_colors.dart';
import 'package:interview_nals/features/shared/ui_dimens.dart';
import 'package:interview_nals/features/weather/domain/entities/weather.dart';
import 'package:interview_nals/features/weather/presentation/viewmodels/weather_view_model.dart';
import 'package:interview_nals/features/weather/presentation/widgets/ui_calendar.dart';
import 'package:interview_nals/features/weather/presentation/widgets/weather_data.dart';
import 'package:interview_nals/features/weather/presentation/widgets/weather_loading.dart';
import 'package:interview_nals/features/weather/presentation/widgets/weather_no_data.dart';
import 'package:interview_nals/injections/weather_injection.dart';
import 'package:provider/provider.dart';

class WeatherPage extends StatefulWidget {
  const WeatherPage({Key? key}) : super(key: key);

  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  late WeatherViewModel _weatherViewModel;
  late UIWeatherDimens dimens;

  @override
  void initState() {
    // init
    dimens = DimensManager.instance.weatherDimens;
    _weatherViewModel = serviceLocator<WeatherViewModel>()..onInitView();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _weatherViewModel,
      builder: (ctx, _) {
        return Scaffold(
          key: UISnackBar.instance.key,
          body: UIDimenTheme(
            dimens: dimens,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                UIStatusBar(backgroundColor: UIColors.calendar),
                _buildCalendar(ctx),
                _buildWeatherBody(ctx),
                UIHomeIndicatorBar(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildCalendar(BuildContext ctx) {
    final selectedDate = ctx.select<WeatherViewModel, DateTime>((vm) {
      return vm.currentDate;
    });
    return UICalendar(
      currentDate: DateTime.now(),
      selectedDate: selectedDate,
      viewModel: ctx.read(),
    );
  }

  Widget _buildWeatherBody(BuildContext ctx) {
    final isLoading = ctx.select<WeatherViewModel, bool>((vm) {
      return vm.viewState != ViewState.success;
    });

    if (isLoading) {
      return WeatherLoading();
    }

    final weather = ctx.select<WeatherViewModel, Weather?>((vm) {
      return vm.weather;
    });

    return Expanded(
      child: RefreshIndicator(
        onRefresh: () async {
          _weatherViewModel.onRefresh();
        },
        child: Stack(
          children: [
            if (weather != null) WeatherData(weather: weather),
            if (weather == null) WeatherNoData(),
            ListView(padding: EdgeInsets.zero),
          ],
        ),
      ),
    );
  }
}
