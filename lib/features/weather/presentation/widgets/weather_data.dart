import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_animation_widget.dart';
import 'package:interview_nals/features/components/ui_counter_up_number.dart';
import 'package:interview_nals/features/components/ui_dimen_theme.dart';
import 'package:interview_nals/features/components/ui_faded_widget.dart';
import 'package:interview_nals/features/components/ui_moving_widget.dart';
import 'package:interview_nals/features/components/ui_rotate_widget.dart';
import 'package:interview_nals/features/components/ui_slide_widget.dart';
import 'package:interview_nals/features/components/ui_text.dart';
import 'package:interview_nals/features/shared/ui_asset_images.dart';
import 'package:interview_nals/features/shared/ui_colors.dart';
import 'package:interview_nals/features/shared/ui_dimens.dart';
import 'package:interview_nals/features/utils/date_time_utils.dart';
import 'package:interview_nals/features/weather/domain/entities/weather.dart';
import 'package:interview_nals/features/weather/presentation/widgets/weather_attribute.dart';

class WeatherData extends StatelessWidget {
  final Weather weather;

  const WeatherData({
    Key? key,
    required this.weather,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dimens = UIDimenTheme.of(context).dimens;
    return Column(
      children: [
        _buildSpacer(),
        _buildAbbreviation(state: weather.abbr, dimens: dimens),
        _buildTemperature(temperature: weather.temp),
        _buildSpacer(),
        _buildName(name: weather.name),
        _buildSizeBox(height: dimens.setHeight(28.0)),
        _buildDate(date: weather.applicableDate),
        _buildSizeBox(height: dimens.setHeight(28.0)),
        _buildAttrs(
          humility: weather.humidity,
          predictability: weather.predictability,
          dimens: dimens,
        ),
        _buildSpacer(),
      ],
    );
  }

  Widget _buildSpacer() {
    return Spacer();
  }

  Widget _buildAbbreviation({
    required String state,
    required BaseDimens dimens,
  }) {
    return UIFadedInAndSlideDownAnimation(
      key: UniqueKey(),
      child: Container(
        height: dimens.setHeight(180.0),
        child: UIAssetImages.isSun(state)
            ? UIRotateWidget(
                child: Image.asset(
                  UIAssetImages.parse(state),
                  fit: BoxFit.contain,
                ),
              )
            : UIMovingWidget(
                child: Image.asset(
                  UIAssetImages.parse(state),
                  fit: BoxFit.contain,
                ),
              ),
      ),
    );
  }

  Widget _buildTemperature({required double temperature}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        UICounterUpNumber(
          key: UniqueKey(),
          end: temperature.toInt(),
          suffix: "\u00B0",
          style: TextStyle(
            fontSize: 92.0,
            fontWeight: FontWeight.w600,
            color: UIColors.calendar,
          ),
        ),
        UIText(
          "C",
          style: TextStyle(
            fontSize: 50.0,
            fontWeight: FontWeight.w600,
            color: UIColors.calendar,
          ),
          strutStyle: StrutStyle(
            fontSize: 92.0,
            fontWeight: FontWeight.w600,
          ),
        )
      ],
    );
  }

  Widget _buildSizeBox({double height = 0, double width = 0}) {
    return SizedBox(height: height, width: width);
  }

  Widget _buildDate({required DateTime date}) {
    return UIText(
      date.toDateOfWeek(),
      style: TextStyle(
        fontSize: 28.0,
        fontWeight: FontWeight.w400,
      ),
    );
  }

  Widget _buildName({required String name}) {
    return FittedBox(
      child: UIText(
        name,
        style: TextStyle(
          fontSize: 52.0,
          fontWeight: FontWeight.w600,
          color: UIColors.lightBlue,
        ),
      ),
    );
  }

  Widget _buildAttrs({
    required int humility,
    required int predictability,
    required BaseDimens dimens,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        WeatherAttribute(
          key: UniqueKey(),
          name: "Humility",
          value: humility,
          backgroundColor: UIColors.lightBlue,
        ),
        SizedBox(width: dimens.setWidth(24.0)),
        WeatherAttribute(
          key: UniqueKey(),
          name: "Predictability",
          value: predictability,
          backgroundColor: UIColors.purple,
        ),
      ],
    );
  }
}
