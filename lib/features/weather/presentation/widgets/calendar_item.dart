import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_text.dart';
import 'package:interview_nals/features/shared/ui_colors.dart';
import 'package:interview_nals/features/utils/date_time_utils.dart';
import 'package:intl/intl.dart';

class CalendarItem extends StatelessWidget {
  final DateTime dayOfWeek;
  final bool isSelected;
  final bool isToday;

  const CalendarItem({
    Key? key,
    required this.dayOfWeek,
    this.isSelected = false,
    this.isToday = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: (!isSelected) ? UIColors.calendar : null,
        gradient: (isSelected)
            ? LinearGradient(
                colors: [UIColors.calendar, UIColors.lightBlue],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              )
            : null,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FittedBox(
            child: UIText(
              (isToday)
                  ? "Today"
                  : "${DateFormat.E(Intl.defaultLocale).format(dayOfWeek)}",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: UIColors.white,
              ),
            ),
          ),
          SizedBox(height: 8.0),
          UIText(
            "${dayOfWeek.month}/${DateTimeUtils.twoDigits(dayOfWeek.day)}",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w600,
              color: UIColors.white,
            ),
          ),
        ],
      ),
    );
  }
}
