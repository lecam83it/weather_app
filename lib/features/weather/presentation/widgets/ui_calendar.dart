import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_dimen_theme.dart';
import 'package:interview_nals/features/shared/ui_colors.dart';
import 'package:interview_nals/features/weather/presentation/viewmodels/weather_view_model.dart';
import 'package:interview_nals/features/weather/presentation/widgets/calendar_item.dart';
import 'package:table_calendar/table_calendar.dart';

class UICalendar extends StatelessWidget {
  final WeatherViewModel viewModel;
  final DateTime currentDate;
  final DateTime selectedDate;

  const UICalendar({
    Key? key,
    required this.viewModel,
    required this.currentDate,
    required this.selectedDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dimens = UIDimenTheme.of(context).dimens;
    return Container(
      color: UIColors.calendar,
      child: TableCalendar(
        onDaySelected: (DateTime selectedDay, DateTime focusedDay) {
          viewModel.updateSelectedDate(selectedDay);
        },
        onPageChanged: (DateTime focusedDay) {
          Future.delayed(Duration(milliseconds: 300), () {
            viewModel.updateSelectedDate(focusedDay);
          });
        },
        firstDay: DateTime.now().subtract(Duration(days: DateTime.daysPerWeek)),
        lastDay: DateTime.now().add(Duration(days: DateTime.daysPerWeek * 2 - 1)),
        calendarFormat: CalendarFormat.week,
        availableGestures: AvailableGestures.horizontalSwipe,
        startingDayOfWeek: StartingDayOfWeek.values[currentDate.weekday - 1],
        headerVisible: false,
        selectedDayPredicate: (day) {
          return isSameDay(selectedDate, day);
        },
        daysOfWeekVisible: false,
        rowHeight: dimens.setHeight(130.0),
        focusedDay: selectedDate,
        calendarBuilders: CalendarBuilders(
          outsideBuilder: (_, dayOfWeek, __) {
            return CalendarItem(dayOfWeek: dayOfWeek);
          },
          selectedBuilder: (_, dayOfWeek, __) {
            return CalendarItem(
              dayOfWeek: dayOfWeek,
              isSelected: true,
              isToday: isSameDay(dayOfWeek, currentDate),
            );
          },
          todayBuilder: (_, DateTime dayOfWeek, __) {
            return CalendarItem(dayOfWeek: dayOfWeek, isToday: true);
          },
          defaultBuilder: (_, dayOfWeek, __) {
            return CalendarItem(dayOfWeek: dayOfWeek);
          },
        ),
      ),
    );
  }
}
