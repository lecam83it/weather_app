import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_circular_percent_indicator.dart';
import 'package:interview_nals/features/components/ui_counter_up_number.dart';
import 'package:interview_nals/features/components/ui_dimen_theme.dart';
import 'package:interview_nals/features/components/ui_text.dart';
import 'package:interview_nals/features/shared/ui_colors.dart';

class WeatherAttribute extends StatelessWidget {
  final String name;
  final int value;
  final Color backgroundColor;

  const WeatherAttribute({
    Key? key,
    required this.name,
    required this.value,
    required this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dimens = UIDimenTheme.of(context).dimens;
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(dimens.setWidth(268)),
      ),
      padding: EdgeInsets.all(dimens.setWidth(40.0)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          UICircularPercentIndicator(
            percent: value,
            size: dimens.setRadius(200),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              UICounterUpNumber(
                key: UniqueKey(),
                end: value,
                style: TextStyle(
                  color: UIColors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              UIText(
                "%",
                style: TextStyle(
                  color: UIColors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
                strutStyle: StrutStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          SizedBox(height: dimens.setHeight(16.0)),
          UIText(
            name,
            style: TextStyle(
              color: UIColors.white,
              fontSize: 14,
            ),
          ),
          SizedBox(height: dimens.setHeight(16.0)),
        ],
      ),
    );
  }
}
