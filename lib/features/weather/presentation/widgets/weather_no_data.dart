import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_text.dart';

class WeatherNoData extends StatelessWidget {
  const WeatherNoData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: UIText(
        "No Data",
        style: TextStyle(fontSize: 50),
      ),
    );
  }
}
