import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_snackbar.dart';
import 'package:interview_nals/features/weather/domain/entities/weather.dart';
import 'package:interview_nals/features/weather/domain/repositories/weather_repository.dart';

enum ViewState {
  idle,
  busy,
  success,
}

class WeatherViewModel with ChangeNotifier {
  final WeatherRepository repository;

  WeatherViewModel({required this.repository});

  late ViewState _viewState;

  ViewState get viewState => _viewState;

  void setState(ViewState viewState) {
    _viewState = viewState;
    notifyListeners();
  }

  void updateUI() {
    notifyListeners();
  }

  late DateTime _currentDate;

  DateTime get currentDate => _currentDate;

  void updateSelectedDate(DateTime selectedDay) {
    if (DateUtils.isSameDay(_currentDate, selectedDay)) return;
    if (_viewState != ViewState.success) return;

    _currentDate = selectedDay;
    _fetchWeather();
  }

  Weather? _weather;

  Weather? get weather => _weather;

  void onInitView() {
    _viewState = ViewState.idle;
    _currentDate = DateTime.now();
    _fetchWeather();
  }

  void _fetchWeather() async {
    setState(ViewState.busy);
    try {
      Weather? weather = await getWeather();
      _weather = weather;
    } catch (e) {
      _weather = null;
      UISnackBar.instance.showError(e.toString());
    }
    setState(ViewState.success);
  }

  Future<Weather?> getWeather() async {
    return repository.getWeather(_currentDate);
  }

  void onRefresh() async {
    if (_viewState != ViewState.success) return;
    _fetchWeather();
  }
}
