import 'package:interview_nals/core/usecases/usecase.dart';
import 'package:interview_nals/features/weather/domain/entities/weather.dart';
import 'package:interview_nals/features/weather/domain/repositories/weather_repository.dart';

class DateParams extends Params {
  final DateTime date;

  DateParams({required this.date});
}

class GetWeather extends UseCase<Weather?, DateParams>{
  final WeatherRepository repository;

  GetWeather(this.repository);

  @override
  Future<Weather?> call(DateParams params) async {
    return await repository.getWeather(params.date);
  }
}