class Weather {
  final int id;
  final String name;
  final String abbr;
  final DateTime applicableDate;
  final double temp;
  final int humidity;
  final int predictability;

  Weather({
    required this.id,
    required this.name,
    required this.abbr,
    required this.applicableDate,
    required this.temp,
    required this.humidity,
    required this.predictability,
  });
}
