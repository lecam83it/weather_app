import 'package:interview_nals/features/weather/domain/entities/weather.dart';

abstract class WeatherRepository {
  Future<Weather?> getWeather(DateTime date);
}