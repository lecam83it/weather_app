class UIConfig {
  UIConfig._();
  // duration
  static const int kProgressAnimationDuration = 1700;
  static const int kCountUpAnimationDuration = 1700;
  static const int kSlideDownAnimationDuration = 1700;
  static const int kFadedInAnimationDuration = 1700;
  static const int kRotateAnimationDuration = 3400;
  // api url
}