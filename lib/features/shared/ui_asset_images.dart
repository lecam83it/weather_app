class UIAssetImages {
  UIAssetImages._();

  static const String clear = "assets/weather_icons/c.png";
  static const String lightCloud = "assets/weather_icons/lc.png";
  static const String heavyCloud = "assets/weather_icons/hc.png";
  static const String showers = "assets/weather_icons/s.png";
  static const String lightRain = "assets/weather_icons/lr.png";
  static const String heavyRain = "assets/weather_icons/hr.png";
  static const String thunderstorm = "assets/weather_icons/t.png";
  static const String hail = "assets/weather_icons/h.png";
  static const String sleet = "assets/weather_icons/sl.png";
  static const String snow = "assets/weather_icons/sn.png";

  static String parse(String abbr) {
    switch(abbr) {
      case "sn":
        return snow;
      case "sl":
        return sleet;
      case "h":
        return hail;
      case "t":
        return thunderstorm;
      case "hr":
        return heavyRain;
      case "lr":
        return lightRain;
      case "s":
        return showers;
      case "hc":
        return heavyCloud;
      case "lc":
        return lightCloud;
      case "c":
      default:
        return clear;
    }
  }

  static bool isSun(String state) {
    return state == "c"; // clear
  }
}









