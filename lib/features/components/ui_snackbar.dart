import 'package:flutter/material.dart';

class UISnackBar {
  late GlobalKey<ScaffoldState> key;

  UISnackBar._() {
    key = GlobalKey();
  }

  static UISnackBar? _instance;

  static UISnackBar get instance {
    if (_instance == null) {
      _instance = UISnackBar._();
    }
    return _instance!;
  }

  showError(String message) {
    var snackBar = new SnackBar(
      content: new Text(message),
      duration: Duration(milliseconds: 1700),
    );
    ScaffoldMessenger.of(key.currentContext!).showSnackBar(snackBar);
  }
}
