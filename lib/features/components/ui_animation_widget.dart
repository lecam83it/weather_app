import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_faded_widget.dart';
import 'package:interview_nals/features/components/ui_slide_widget.dart';

class UIFadedInAndSlideDownAnimation extends StatelessWidget {
  final Widget child;

  const UIFadedInAndSlideDownAnimation({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UISlideWidget(child: UIFadedWidget(child: child));
  }
}
