import 'package:flutter/material.dart';
import 'package:interview_nals/features/shared/ui_dimens.dart';

class UIDimenTheme extends InheritedWidget {
  final BaseDimens dimens;
  const UIDimenTheme({
    Key? key,
    required this.dimens,
    required Widget child,
  }) : super(key: key, child: child);

  static UIDimenTheme of(BuildContext context) {
    final UIDimenTheme? result = context.dependOnInheritedWidgetOfExactType<UIDimenTheme>();
    assert(result != null, 'No UIDimenTheme found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(UIDimenTheme old) {
    return dimens != old.dimens;
  }
}
