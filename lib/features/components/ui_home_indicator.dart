import 'package:flutter/material.dart';
import 'package:interview_nals/features/shared/ui_dimens.dart';

class UIHomeIndicatorBar extends StatelessWidget {
  final Color backgroundColor;

  const UIHomeIndicatorBar({
    Key? key,
    this.backgroundColor = Colors.transparent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: DimensManager.instance.weatherDimens.indicatorBarHeight,
      width: DimensManager.instance.weatherDimens.fullWidthSafeArea,
      color: backgroundColor,
    );
  }
}
