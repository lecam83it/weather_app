import 'dart:async';

import 'package:flutter/material.dart';
import 'package:interview_nals/features/config/ui_config.dart';

class UIMovingWidget extends StatefulWidget {
  final Widget child;

  // delay time (milliseconds)
  final int delay;

  const UIMovingWidget({
    Key? key,
    required this.child,
    this.delay = UIConfig.kFadedInAnimationDuration,
  }) : super(key: key);

  @override
  _UIMovingWidgetState createState() => _UIMovingWidgetState();
}

class _UIMovingWidgetState extends State<UIMovingWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Offset> _offsetAnimation;

  final Offset begin = Offset(-0.1, 0.0);
  final Offset end = Offset(0.1, 0.0);

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: UIConfig.kRotateAnimationDuration),
      vsync: this,
    );

    _offsetAnimation = TweenSequence([
      TweenSequenceItem(tween: Tween<Offset>(begin: begin, end: end), weight: 1),
      TweenSequenceItem(tween: Tween<Offset>(begin: end, end: begin), weight: 1),
    ]).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));

    // delay rotation animation
    Timer(Duration(milliseconds: widget.delay), () {
      _controller.repeat();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _offsetAnimation,
      child: widget.child,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
