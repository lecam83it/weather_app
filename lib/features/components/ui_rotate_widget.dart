import 'dart:async';

import 'package:flutter/material.dart';
import 'package:interview_nals/features/config/ui_config.dart';

class UIRotateWidget extends StatefulWidget {
  final Widget child;
  // delay time (milliseconds)
  final int delay;

  const UIRotateWidget({
    Key? key,
    required this.child,
    this.delay = UIConfig.kFadedInAnimationDuration,
  }) : super(key: key);

  @override
  _UIRotateWidgetState createState() => _UIRotateWidgetState();
}

class _UIRotateWidgetState extends State<UIRotateWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: UIConfig.kRotateAnimationDuration),
      vsync: this,
    );

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));

    // delay rotation animation
    Timer(Duration(milliseconds: widget.delay), () {
      _controller.repeat();
    });
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: _animation,
      child: widget.child,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
