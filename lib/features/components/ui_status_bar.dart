import 'package:flutter/material.dart';
import 'package:interview_nals/features/shared/ui_dimens.dart';

class UIStatusBar extends StatelessWidget {
  final Color backgroundColor;

  const UIStatusBar({
    Key? key,
    this.backgroundColor = Colors.transparent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: DimensManager.instance.weatherDimens.statusBarHeight,
      width: DimensManager.instance.weatherDimens.fullWidthSafeArea,
      color: backgroundColor,
    );
  }
}
