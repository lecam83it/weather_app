import 'package:flutter/material.dart';
import 'package:interview_nals/features/shared/ui_fonts.dart';

class UIText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final StrutStyle? strutStyle;
  final int? maxLines;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final bool? softWrap;

  UIText(this.text, {
    Key? key,
    this.style,
    this.strutStyle,
    this.maxLines,
    this.textAlign,
    this.overflow,
    this.softWrap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textScaleFactor: 1.0,
      maxLines: maxLines,
      style: TextStyle(
        fontSize: 14,
        letterSpacing: 0.75,
        height: 1.25,
        fontFamily: UIFonts.kOpenSans,
      ).merge(style),
      strutStyle: strutStyle,
      textAlign: textAlign,
      softWrap: softWrap,
      overflow: overflow,
    );
  }
}
