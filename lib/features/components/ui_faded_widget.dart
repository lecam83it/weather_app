import 'package:flutter/material.dart';
import 'package:interview_nals/features/config/ui_config.dart';

class UIFadedWidget extends StatefulWidget {
  final Widget child;

  const UIFadedWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  _UIFadedWidgetState createState() => _UIFadedWidgetState();
}

class _UIFadedWidgetState extends State<UIFadedWidget> with SingleTickerProviderStateMixin {

  late AnimationController _controller;

  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: UIConfig.kFadedInAnimationDuration),
      vsync: this,
    )..forward();

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _animation,
      child: widget.child,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
