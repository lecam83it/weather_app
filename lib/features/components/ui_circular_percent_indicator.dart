import 'dart:math';

import 'package:flutter/material.dart';
import 'package:interview_nals/features/config/ui_config.dart';
import 'package:interview_nals/features/shared/ui_colors.dart';

class UICircularPercentIndicator extends StatelessWidget {
  final int percent;
  final double size;

  const UICircularPercentIndicator({
    Key? key,
    required this.percent,
    required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
      size: size,
      percent: percent,
    );
  }
}

class CircularPercentIndicator extends StatefulWidget {
  final double size;
  final int percent;

  const CircularPercentIndicator({
    required this.size,
    required this.percent,
  });

  @override
  _CircularPercentIndicatorState createState() =>
      _CircularPercentIndicatorState();
}

class _CircularPercentIndicatorState extends State<CircularPercentIndicator>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: UIConfig.kProgressAnimationDuration),
    )..forward();
    _animation = IntTween(
      begin: 0,
      end: widget.percent,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          key: UniqueKey(),
          height: widget.size,
          width: widget.size,
          child: AnimatedBuilder(
            animation: _animation,
            builder: (_, __) {
              return CustomPaint(
                painter: CircularIndicator(percent: _animation.value),
              );
            },
          ),
        ),
        Container(
          height: widget.size * 0.8,
          width: widget.size * 0.8,
          decoration: BoxDecoration(
            color: UIColors.white,
            borderRadius: BorderRadius.circular(widget.size * 0.8),
          ),
        )
      ],
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class CircularIndicator extends CustomPainter {
  final int percent;

  CircularIndicator({
    required this.percent,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Offset center = Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    Rect circle = Rect.fromCircle(center: center, radius: radius);
    final double maxAngle = 0.8;
    double angle = 2 * pi * maxAngle;

    Paint outerPaint = Paint()
      ..strokeWidth = size.width * 0.05
      ..color = Colors.white.withOpacity(0.5)
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(circle, (3 * pi - angle) / 2, angle, false, outerPaint);

    Paint progressPaint = Paint()
      ..strokeWidth = size.width * 0.075
      ..color = Colors.white
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(circle, (3 * pi - angle) / 2, angle * percent / 100, false,
        progressPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
