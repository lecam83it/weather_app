import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_text.dart';
import 'package:interview_nals/features/config/ui_config.dart';

class UICounterUpNumber extends StatefulWidget {
  final int begin;
  final int end;
  final TextStyle? style;
  final String suffix;

  const UICounterUpNumber({
    Key? key,
    this.begin = 0,
    required this.end,
    this.style,
    this.suffix = "",
  }) : super(key: key);

  @override
  _UICounterUpNumberState createState() => _UICounterUpNumberState();
}

class _UICounterUpNumberState extends State<UICounterUpNumber>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: UIConfig.kCountUpAnimationDuration),
      vsync: this,
    )..forward();

    _animation = IntTween(
      begin: widget.begin,
      end: widget.end,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return _AnimatedText(
      key: UniqueKey(),
      animation: _animation,
      style: widget.style,
      suffix: widget.suffix,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class _AnimatedText extends AnimatedWidget {
  final Animation animation;
  final TextStyle? style;
  final String suffix;

  _AnimatedText({
    Key? key,
    required this.animation,
    this.style,
    this.suffix = "",
  }) : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    return UIText(
      this.animation.value.toInt().toString() + "$suffix",
      style: this.style,
    );
  }
}
