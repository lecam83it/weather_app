import 'package:flutter/material.dart';
import 'package:interview_nals/features/config/ui_config.dart';

class UISlideWidget extends StatefulWidget {
  final Widget child;

  const UISlideWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  _UISlideWidgetState createState() => _UISlideWidgetState();
}

class _UISlideWidgetState extends State<UISlideWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Offset> _offsetAnimation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: UIConfig.kSlideDownAnimationDuration),
      vsync: this,
    )..forward();

    _offsetAnimation = Tween<Offset>(
      begin: const Offset(0.0, -0.5),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _offsetAnimation,
      child: widget.child,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
