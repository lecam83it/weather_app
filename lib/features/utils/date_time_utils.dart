import 'package:intl/intl.dart';

class DateTimeUtils {
  DateTimeUtils._();

  static String twoDigits(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }

  static bool isSameDate(DateTime current, DateTime other) {
    return current.year == other.year &&
        current.month == other.month &&
        current.day == other.day;
  }
}

extension DateTimeExtension on DateTime {

  String toDateOfWeek() {
    final dayOfWeek = DateFormat.E(Intl.defaultLocale).format(this);
    final day = DateFormat.d(Intl.defaultLocale).format(this);
    final month = DateFormat.MMM(Intl.defaultLocale).format(this);
    final year = DateFormat.y(Intl.defaultLocale).format(this);
    return "$dayOfWeek $month $day, $year";
  }

  String toSlashDate() {
    return "${this.year}/${this.month}/${this.day}";
  }

  DateTime toDate() {
    return DateTime(this.year, this.month, this.day);
  }

  static DateTime parseToDate(String dateTimeString) {
    return DateTime.parse(dateTimeString).toLocal().toDate();
  }
}