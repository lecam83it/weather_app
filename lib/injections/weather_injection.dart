import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:interview_nals/core/database/database_client.dart';
import 'package:interview_nals/core/platform/client.dart';
import 'package:interview_nals/core/platform/network_info.dart';
import 'package:interview_nals/features/weather/data/datasources/weather_local_data_source.dart';
import 'package:interview_nals/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:interview_nals/features/weather/data/repositories/weather_repository_impl.dart';
import 'package:interview_nals/features/weather/domain/repositories/weather_repository.dart';
import 'package:interview_nals/features/weather/domain/usecases/get_weather.dart';
import 'package:interview_nals/features/weather/presentation/viewmodels/weather_view_model.dart';
import 'package:sqflite/sqflite.dart';

final serviceLocator = GetIt.instance;

Future<void> init() async {
  //! Features - Weather App

  //! Use cases
  serviceLocator.registerLazySingleton(() => GetWeather(serviceLocator()));

  //! Repository
  serviceLocator.registerLazySingleton<WeatherRepository>(
    () => WeatherRepositoryImpl(
      networkInfo: serviceLocator(),
      remoteDataSource: serviceLocator(),
      localDataSource: serviceLocator(),
    ),
  );

  //! Network
  serviceLocator.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImpl(connectivity: serviceLocator()),
  );

  //! DataSources
  serviceLocator.registerLazySingleton<WeatherLocalDataSource>(
    () => WeatherLocalDataSourceImpl(database: serviceLocator()),
  );

  serviceLocator.registerLazySingleton<WeatherRemoteDataSource>(
    () => WeatherRemoteDataSourceImpl(client: serviceLocator()),
  );

  serviceLocator.registerLazySingleton<Client>(
    () => Client(dio: serviceLocator()),
  );

  //! External
  final Database db = await DatabaseClient.db;
  serviceLocator.registerLazySingleton<Database>(() => db);

  serviceLocator.registerLazySingleton(() => Dio());
  serviceLocator.registerLazySingleton(() => Connectivity());

  // viewModel
  serviceLocator.registerLazySingleton<WeatherViewModel>(
    () => WeatherViewModel(repository: serviceLocator()),
  );
}
