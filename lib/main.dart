import 'package:flutter/material.dart';
import 'package:interview_nals/features/components/ui_snackbar.dart';
import 'package:interview_nals/features/shared/ui_dimens.dart';
import 'package:interview_nals/features/weather/presentation/pages/weather_page.dart';
import 'package:interview_nals/injections/weather_injection.dart' as di;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    DimensManager();
    return MaterialApp(
      home: WeatherPage(),
    );
  }
}
